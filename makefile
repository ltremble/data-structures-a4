flags := -Wall -std=c99 -pedantic

bin/heap: heap.o
	gcc $(flags) heap.o -o bin/heap

heap.o: heap.c
	gcc $(flags) -c heap.c

clean:
	rm *.o bin/*