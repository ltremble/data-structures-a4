# Data Structures A4

## A4 coding project for data structures

[GitLab Repo](https://git.socs.uoguelph.ca/ltremble/data-structures-a4)

### Author

Luke Tremble

ID: 1167443

2022-10-21

## To compile and Run

Compilation:

```
make
```

To run:

```
./bin/heap
```

## Sample output

### Compilation

```
make
gcc -Wall -std=c99 -pedantic -c heap.c
gcc -Wall -std=c99 -pedantic heap.o -o bin/heap
skywalkertrem@DESKTOP-Q8G6IRB:/mnt/c/Users/skywa/Desktop/DataStructuresCode/data-structures-a4$ /Users/Luke/Desktop/cCode/DataStructures/data-structures-a2#
```

### Heap Sort

```
./bin/heap
1: 64 26 28 10 23 3 32 65 61 28 95 
2: 79 25 0 54 25 19 25 81 21 2 78
3: 83 10 56 17 24 47 2 35 45 19 51
4: 96 31 44 21 79 68 97 32 13 62 80
5: 115 30 73 12 75 14 85 72 60 91 42
6: 117 19 9 89 77 80 75 91 34 61 24
7: 118 77 7 34 93 4 19 10 44 76 19
8: 129 99 30 0 91 26 9 91 70 21 14
9: 130 3 98 29 5 60 15 98 86 4 61
10: 132 83 46 3 67 90 48 4 74 41 52
11: 138 62 30 46 71 41 38 80 60 99 5
12: 143 36 42 65 18 81 93 72 34 59 37
13: 144 86 18 40 47 13 94 98 22 79 94
14: 146 68 37 41 12 6 85 51 85 60 56
15: 150 19 48 83 98 11 30 41 72 9 31
16: 156 77 51 28 24 77 2 36 64 32 5
17: 176 47 44 85 30 84 53 28 42 7 65
18: 203 62 74 67 19 41 35 38 16 9 80
19: 212 61 69 82 25 62 12 83 35 56 19
20: 223 66 63 94 42 77 64 56 80 63 14
skywalkertrem@DESKTOP-Q8G6IRB:/mnt/c/Users/skywa/Desktop/DataStructuresCode/data-structures-a4$
```

