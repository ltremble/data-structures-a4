#include "heap.h"

/**
 * The function takes in a heap and the size of the heap, and sorts the heap in ascending order
 * 
 * @param argc the number of arguments passed to the program
 * @param argv 
 */
int main(int argc, char const *argv[])
{
    node heap[20];

    for (int i = 0; i < 20; i++)
    {
        heap[i].sum_key = 0;
        for (int j = 0; j < 3; j++)
        {
            heap[i].key[j] = 0;
        }
        for (int k = 0; k < 7; k++)
        {
            heap[i].content[k] = 0;
        }
    }
    setTree(heap);
    heapSort(heap, 20);
    printTree(heap);
    exit(0);
}

/**
 * Swap the two nodes in the heap at positions a and b
 * 
 * @param heap the heap array
 * @param a the index of the first node
 * @param b the index of the node to be swapped
 */
void swap(node heap[], int a, int b)
{
    node temp = heap[b];
    heap[b] = heap[a];
    heap[a] = temp;
}

/**
 * It reads in a file, and stores the data in a struct array.
 * 
 * @param heap the array of nodes
 */
void setTree(node heap[])
{
    char line[100];
    FILE *infile = fopen("f.dat", "r");
    int i = 0;
    while (!feof(infile))
    {
        fgets(line, 100, infile);
        if (line != NULL)
        {
            sscanf(line, "%d%d%d%d%d%d%d%d%d%d", &heap[i].key[0], &heap[i].key[1], &heap[i].key[2], &heap[i].content[0],
                   &heap[i].content[1], &heap[i].content[2], &heap[i].content[3], &heap[i].content[4], &heap[i].content[5],
                   &heap[i].content[6]);
            heap[i].sum_key = heap[i].key[0] + heap[i].key[1] + heap[i].key[2];
            i++;
        }
    }
}

/**
 * It prints the contents of the heap array
 * 
 * @param heap the array of nodes
 */
void printTree(node heap[])
{
    for (int i = 0; i < 20; i++)
    {
        printf("%d: ", i + 1);
        printf("%d ", heap[i].sum_key);
        for (int j = 0; j < 3; j++)
        {
            printf("%d ", heap[i].key[j]);
        }
        for (int k = 0; k < 7; k++)
        {
            printf("%d ", heap[i].content[k]);
        }
        printf("\n");
    }
}

/**
 * The heapify function takes an array, the size of the array, and the index of the root node as input.
 * It then compares the root node with its left and right child nodes. If the root node is smaller than
 * either of its child nodes, it swaps the root node with the larger of the two child nodes. It then
 * calls itself recursively on the child node that was swapped with the root node
 * 
 * @param arr The array to be sorted
 * @param n The number of elements in the array
 * @param i index of the node
 */
void heapify(node arr[], int n, int i)
{
    // Find largest among root, left child and right child
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && arr[left].sum_key > arr[largest].sum_key)
        largest = left;

    if (right < n && arr[right].sum_key > arr[largest].sum_key)
        largest = right;

    // Swap and continue heapifying if root is not largest
    if (largest != i)
    {
        swap(arr, largest, i);
        heapify(arr, n, largest);
    }
}

/**
 * We first build a max heap from the input data. After the max heap is built, remove the root and move it to the end
 * then heapify the array again, repeat for every element in the array.
 * 
 * @param arr The array to be sorted
 * @param n The number of elements in the array
 */
void heapSort(node arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    for (int i = n - 1; i >= 0; i--)
    {
        swap(arr, 0, i);

        heapify(arr, i, 0);
    }
}
