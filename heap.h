#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node{
    int sum_key;
    int key[3];
    int content[7];
} node;


void setTree(node heap[]);

void printTree(node heap[]);

void downHeapAll(node heap[]);

void heapify(node arr[], int n, int i);

void heapSort(node heap[], int n);

void swap(node heap[], int a, int b);